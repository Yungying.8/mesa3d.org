---
title:    "November 13, 2007"
date:     2007-11-13 00:00:00
category: misc
tags:     []
summary:  "Gallium3D is the codename for the new Mesa device driver architecture
which is currently under development."
---
Gallium3D is the codename for the new Mesa device driver architecture
which is currently under development.

Gallium3D development is taking place on the *gallium-0.1* branch of the
git repository. Currently, there's only a software-only driver and an
Intel i915/945 driver but other drivers will be coming...
