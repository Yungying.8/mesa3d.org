---
title:    "Mesa 20.0.4 is released"
date:     2020-04-03 00:00:00
category: releases
tags:     []
---
[Mesa 20.0.4](https://docs.mesa3d.org/relnotes/20.0.4.html) is released. This is an emergency
release which reverts a serious SPIR-V regression in the 20.0.3 release.
Users of 20.0.3 are recommended to upgrade immediately.
